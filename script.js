
let tabsContent = document.querySelectorAll('[data-tab-content]');


let tabs = document.querySelectorAll('[data-target]');

tabs.forEach(tab =>{
    tab.addEventListener('click',() => {
        tabs.forEach(tab =>{
            tab.classList.remove('active');
        });
        let target = document.getElementById(tab.dataset.target);
        target.classList.add('active');
        tabsContent.forEach(tabContent => {
            tabContent.classList.remove('active');

        });
        let targetContent = document.getElementById(tab.dataset.target.slice(1,tab.dataset.target.length));
        targetContent.classList.add('active');
    })
});
